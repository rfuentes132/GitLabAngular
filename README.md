# GitLab API

Dia 1
Autenticación
Listar
    *Grupos a los que pertenezco
    *Proyectos

Dia 2
Proyectos
	Listar, crear, eliminar y ver el detalle.
	Listar los branches
Grupos
	Crear y eliminar grupo, listar los miembros de los grupos y poder agregar los miembros a los grupos.
	Crear directiva para poder agregar personas con un campo autocompletable
	
Dia 3
    Obtener los archivos de los proyectos y mostrar su contenido
    Directorios o simplemente archivos planos.
    
Dia 4
    Aplicar todas las opciones de branches
    https://github.com/gitlabhq/gitlabhq/blob/master/doc/api/branches.md