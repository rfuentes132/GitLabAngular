(function() {
  'use strict';

  angular
    .module('gitLabAngular', ['ngAnimate', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'toastr']);

})();
