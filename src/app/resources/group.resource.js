'use strict';

angular
  .module('gitLabAngular')
  .factory('Group', function (API, $resource) {
    return $resource(
      API.groups,
      {id: '@id'},
      {
        update: {
          method: 'put'
        }
      });
  });