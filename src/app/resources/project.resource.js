'use strict';

angular
  .module('gitLabAngular')
  .factory('Project', function (API, $resource) {
    return $resource(
      API.projects,
      {id: '@id'},
      {
        update: {
          method: 'put'
        }
      });
  });