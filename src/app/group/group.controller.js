'use strict';

angular
.module('gitLabAngular')
.controller('GroupController', function ($http, $state, API, Token, Group) {

  var vm = this;
  vm.groups = Group.query();
  vm.gr = {};

  vm.list = function () {
    console.log(vm.groups);
  }

  vm.toggle = function () {
    return true;
  }


  vm.add = function () {
    console.log(vm.gr);
    Group.save(vm.gr, function (response) {
      $('#addForm').modal('toggle');
      vm.groups.push(vm.gr);
      console.log(vm.gr);
      console.log(response);
      vm.gr = {};
    }, function () {
      console.log('Name already taken');
    });
  }

  vm.remove = function (index) {
    console.log("remove item");
    Group.delete(vm.groups[index], function (response) {
      console.log(response);
      vm.groups.splice(index, 1);
    });
  }
});
