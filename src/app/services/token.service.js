'use strict';

angular
  .module('gitLabAngular')
  .service('Token', function ($window) {

    this.set = function (user) {
      // $window.sessionStorage.setItem('user', JSON.stringify(user));
      $window.sessionStorage.setItem('user', user);
    };

    this.get = function () {
      return JSON.parse($window.sessionStorage.getItem('user'));
    };

    this.has = function () {
      return !!$window.sessionStorage.getItem('user');
    };

    this.delete = function () {
      $window.sessionStorage.removeItem('user', user);
    }

  });