'use strict';

angular
  .module('gitLabAngular')
  .service('API', function (baseUrl) {

    var toUrl = function (path) {
      return baseUrl+path;
    }

    this.projects = toUrl('/projects/:id');
    // this.access = toUrl('/oauth/token');
    this.access = toUrl('/session');
    this.groups = toUrl('/groups/:id');
    // this.groups = toUrl('/groups');
  });




  // POST /oauth/token
  // POST /session
  // GET /projects
  // GET /groups
