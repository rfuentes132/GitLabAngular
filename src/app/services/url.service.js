'use strict';

angular
  .module('gitLabAngular')
  .constant('baseUrl', 'https://gitlab.com/api/v3');