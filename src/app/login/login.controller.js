'use strict';

angular
  .module('gitLabAngular')
  .controller('LoginController', function ($http, $state, API, Token) {

    var vm = this;
    vm.user = {};

    vm.access = function () {
      // console.log(vm.user);
      if (vm.user.username && vm.user.password) {

        console.log('Loggeando usuario');
        $http({
          method: 'POST',
          url: API.access,
          data:{
            email: vm.user.username,
            password: vm.user.password
          }
        }).success(function (data){
          Token.set(data.private_token);
          console.log('loging');
          console.log(data);
          $state.go('projects');
        })
      }
    };

  });
