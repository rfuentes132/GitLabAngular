'use strict';

angular
.module('gitLabAngular')
.controller('ProjectController', function ($http, $state, API, Token, Project) {

	var vm = this;
	vm.pr = {
		visibility_level: 0
	};
	vm.projects = Project.query();

	vm.list = function () {
		console.log("pewpew");
		console.log(vm.projects);
	}

	vm.toggle = function () {
		return true;
	}

	vm.add = function () {
		console.log(vm.pr);
		vm.projects.push(Project.save(vm.pr, function (response) {
			console.log(response);
			$('#addForm').modal('toggle');
			vm.pr = {visibility_level: 0};
		}));
	}

	vm.remove = function (index) {
		console.log("remove item");
		Project.delete(vm.projects[index], function (response) {
			console.log(response);
			vm.projects.splice(index, 1);
		});
	}

	vm.perms = function (vis) {
		if (vis == 0) return "Private";
		if (vis == 10) return "Internal";
		if (vis == 20) return "Public";
	}

});
