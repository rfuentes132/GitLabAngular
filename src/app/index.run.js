(function() {
  'use strict';

  angular
    .module('gitLabAngular')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
